#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Inoreader.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Inoreader.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<linearGradient id="a" x1="5.98" x2="57.28" y1="1047" y2="994" gradientTransform="matrix(4.651 0 0 4.615 -19.33 -4582)" gradientUnits="userSpaceOnUse">
<stop stop-color="#367299" offset="0"/>
<stop stop-color="#4da1d9" offset="1"/>
</linearGradient>
<filter id="b" x="-.05895" y="-.05888" width="1.118" height="1.124" color-interpolation-filters="sRGB">
<feFlood flood-color="rgb(0,0,0)" flood-opacity=".502" in="SourceGraphic" result="flood"/>
<feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="4.000000"/>
<feOffset dx="0.000000" dy="1.000000" in="blur" result="offset"/>
<feComposite in="flood" in2="offset" operator="in" result="comp1"/>
<feComposite in="SourceGraphic" in2="comp1" result="comp2"/>
</filter>
</defs>
<path d="m128.1 2.641c-30.64-.01-76.87 5.19-98.54 26.84-21.68 21.65-26.91 67.87-26.92 98.51-.01 30.64 5.19 76.86 26.84 98.54 21.65 21.68 67.87 26.91 98.51 26.92 30.64.01 76.87-5.18 98.54-26.84 21.68-21.65 26.91-67.87 26.92-98.51.01-30.64-5.19-76.86-26.84-98.54-21.65-21.68-67.87-26.91-98.51-26.92z" fill="url(#a)"/>
<path d="m107.8 44.08-.004.02133c-2.051.003-5.223.02933-5.223.02933.0209 5.901.0198 11.78 0 16 7.23-.3209 14.56.1833 21.87 1.525 46.89 8.598 80.03 51.01 77.79 98.36 3.755 5e-5 11.21-.00011 17.05.008 1.398-30.77-10.09-61.22-32.64-83.65-17.84-17.74-39.08-28.2-64.64-31.81-2.322-.3278-7.23-.4919-14.2-.4824zm-3.672 35.23c-.5531.0095-1.106.02362-1.658.04491.1251 7.325.1241 12.56.0586 17.34.798-.01333 1.692-.01409 2.738-.01371 6.144.0025 9.554.405 15.05 1.785 17.25 4.327 31.95 16.29 39.85 32.45 3.962 8.099 5.923 16.08 6.182 25.2.0373 1.313.0351 2.622-.008 3.92 5.474 0 11.64.00041 17.32-.0293.0424-4.049-.0686-8.9-.3125-10.95-1.504-12.66-5.55-23.92-12.29-34.22-11.5-17.57-29.5-29.81-49.9-33.95-5.653-1.145-11.35-1.661-17.02-1.563zm-2.27 34.19c-3.847.0941-6.663.4658-10.17 1.346-16.98 4.259-30.26 18-34.07 35.24-.8085 3.665-1.079 6.255-1.074 10.28.0069 5.444.7142 9.863 2.377 14.85 5.31 15.93 18.84 27.87 35.21 31.07 1.573.3074 3.678.5926 5.74.7773v.006c1.288.1153 6.991-.0316 8.537-.2207 8.719-1.067 16.8-4.477 23.6-9.951 6.346-5.116 11.18-11.7 14.2-19.34 1.936-4.899 2.937-9.588 3.207-15.03.9256-18.68-9.386-36.2-26.11-44.38-6.781-3.315-13.82-4.841-21.45-4.654zm14.2 19.91c1.59-.0386 3.214.2108 4.805.7793 6.844 2.449 10.45 10.03 8.072 16.96-1.432 4.179-4.935 7.437-9.197 8.561h-.002c-1.63.4296-4.39.5038-5.977.1602-4.137-.8952-7.36-3.402-9.203-7.158-1.923-3.92-1.922-8.026.006-11.9 2.271-4.565 6.727-7.285 11.5-7.4z" fill="#f9f9f9" filter="url(#b)" style="paint-order:normal"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Inoreader
Comment=One place to keep up with all your information sources
Keywords=rss;
Exec={{kiosk}} 'https://www.inoreader.com/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=Network;News;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=inoreader \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
