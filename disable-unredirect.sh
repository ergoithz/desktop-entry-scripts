#!/bin/bash -e
: << DESCRIPTION
Generate an autostart desktop file to disable mutter fullscreen unredirect.

License: MIT
DESCRIPTION

# directory definitions
startupDir="$HOME/.config/autostart/"

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Name=Disable Unredirect
Exec=gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell --method org.gnome.Shell.Eval 'Meta.disable_unredirect_for_display(global.display);'
Terminal=true
Type=Application
EOF

mkdir -p "$startupDir"
echo "$desktopFile" > "$startupDir/disable-unredirect.desktop"