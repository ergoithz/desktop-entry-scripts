#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for drawio.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open drawio.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient4615-3" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#e03535" offset="0"/>
<stop stop-color="#df2e2e" offset=".7"/>
<stop stop-color="#ce1f1f" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#radialGradient4615-3)" stroke-width=".2646" style="fill:#f08705"/>
<path d="m161.8 46.45-26.33 27.9-95.32 135 39.35 39.65c16.83 3.33 34.27 4.475 48.41 4.48 30.64.01 76.87-5.18 98.54-26.84 19.42-19.4 25.6-58.43 26.69-88.4l-91.33-91.76z" style="fill:#df6c0c"/>
<path d="m101.1 43.47c-5.39-2e-6-9.74 4.336-9.74 9.726l-1.1e-5 41.79c0 5.39 4.35 9.74 9.74 9.74l1.83-3e-5-26.87 46.55-28.69 3e-5c-5.39 0-9.726 4.336-9.726 9.726l-1.6e-5 41.8c5e-6 5.39 4.336 9.726 9.726 9.726l53.8 5e-5c5.39-6e-5 9.726-4.336 9.726-9.726l1e-5-41.8c0-5.39-4.336-9.726-9.726-9.726l-8.135 1e-5 26.87-46.55 16.2-2e-5 26.88 46.55-8.178 2e-5c-5.39 0-9.726 4.336-9.726 9.726l-2e-5 41.8c0 5.39 4.336 9.726 9.726 9.726l53.81 1e-5c5.39 0 9.726-4.336 9.726-9.726l1e-5-41.8c0-5.39-4.336-9.726-9.726-9.726l-28.71-2e-5-26.88-46.55 1.872-1e-5c5.39 0 9.726-4.35 9.726-9.74l1e-5-41.79c0-5.39-4.336-9.726-9.726-9.726z" style="fill:#f9f9f9"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Draw.io
Comment=Online diagram software for making flowcharts, charts, diagrams
Keywords=drawing;chart;
Exec={{kiosk}} 'https://draw.io'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=Office;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=drawio \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
