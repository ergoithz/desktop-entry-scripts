#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Curiosity Stream.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Curiosity Stream.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient4615" cx="-165.34" cy="-7.2892" r="83.21" gradientTransform="matrix(3.4341 0 -9.7873e-8 3.4281 702.21 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#f6a107" offset="0"/>
<stop stop-color="#f6a107" offset=".7"/>
<stop stop-color="#e07d00" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.862-26.921 98.511-21.67 21.661-67.901 26.851-98.542 26.84-30.64-.01286-76.862-5.245-98.511-26.921-21.65-21.675-26.851-67.901-26.84-98.542.0128-30.64 5.245-76.862 26.921-98.511 21.675-21.65 67.901-26.851 98.542-26.84 30.64.012801 76.862 5.245 98.511 26.921 21.65 21.675 26.851 67.901 26.84 98.542z" fill="url(#radialGradient4615)" stroke-width=".26458"/>
<path d="m140.57 53.414v72.402h-.017l-.17915 2.0323c-1.1944 6.7704-7.5078 11.4-14.324 10.503-6.3119-.8354-11.059-6.1691-11.157-12.535h-.0121v-72.402l-21.758-.09793v72.103h.0068c-.03326 14.179 8.5865 26.943 21.751 32.209v22.745h25.689v-22.63c11.12-4.4448 19.152-14.324 21.233-26.117l.5254-6.0084c-.00061-.0661-.001-.1321-.002-.1982h.002v-72.103zm-13.249 134c-6.2745.20584-11.406 5.0658-11.953 11.32-.57839 6.6158 4.1553 12.514 10.739 13.381 6.5848.86655 12.684-3.6064 13.837-10.147l.18868-2.1541c-8e-5-6.6411-5.2298-12.104-11.864-12.394-.31587-.0136-.6321-.0155-.94808-.005z" fill="#fff" stroke-linejoin="round" stroke-width=".26458" style="-inkscape-stroke:none"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Curiosity Stream
Comment=Documentaries that enlighten, entertain & inspire
Keywords=documentary;streaming;
Exec={{launch}} 'https://curiositystream.com/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=curiositystream \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
