#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for h!anime.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open h!anime.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse"><stop stop-color="#232323" style="stop-color:#252438" offset="0"/>
<stop stop-color="#161616" style="stop-color:#1c1b2b" offset=".7"/>
<stop stop-color="#232323" style="stop-color:#222134" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#a)" stroke-width=".2646"/>
<path d="m89.32 107.7c6.441-5.765 15.1-9.591 24.48-9.591 10.36 0 17.49 3.55 22.88 8.763 5.39 5.214 8.121 16.49 8.191 26.82v51.74h-29.82v-51.51c0-4.762-1.05-8.196-3.15-10.3-2.03-2.156-5.355-3.233-9.976-3.233-5.39 0-9.591 1.78-12.6 5.339v59.71h-29.72v-115.5h29.72z" style="fill:#f9f9f9" aria-label="h"/>
<path d="m201.7 70.14-37.53.08829 1.51 62.91 29.62.5748zm-20.53 75.87a21.21 20.03 0 00-21.22 20.03 21.21 20.03 0 0021.22 20.02 21.21 20.03 0 0021.21-20.02 21.21 20.03 0 00-21.21-20.03z" style="fill:#feb9dd"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=H!anime
Comment=Watch your favorite anime online
Keywords=hianime;anime;streaming;
Exec={{kiosk}} 'https://hianime.to/home'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=hianime \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
