# XDG-Desktop entry scripts

This repository contains scripts that generates desktop entries with various
effects.

They **must be run as regular user** (not root).

## Firefox-based web applications

Scripts prefixed with `firefox-` generates desktop entries along with their own
common custom firefox profile (`firefox-kiosk`) as a shortcut to common web
applications.

Here is a list of the popular ones:

* [firefox-disneyplus.sh](./firefox-disneyplus.sh)
* [firefox-netflix.sh](./firefox-netflix.sh)
* [firefox-primevideo.sh](./firefox-primevideo.sh)
* [firefox-spotify.sh](./firefox-spotify.sh)
* [firefox-telegram.sh](./firefox-telegram.sh)
* [firefox-twitch.sh](./firefox-twitch.sh)
* [firefox-youtube.sh](./firefox-youtube.sh)

Firefox shortcuts will ran in the `firefox-kiosk` profile, customized as:

* No homepage (site URL passed by the shortcut).
* Auto-hide all unnecessary browser UI.
* Avoid mozilla telemetry, spam and legal gibberish tabs.

For more information about these customizations you can take a look at
the generic [firefox-kiosk.sh](./firefox-kiosk.sh) script.

To run all firefox scripts:

```sh
for i in ./firefox-*; do $i; done
```

### Gamescope support (experimental)

If [gamescope](https://github.com/ValveSoftware/gamescope) is detected
during installation, a `firefox-kiosk-script` wrapper will be used
to conditionally apply gamescope-specific configuration on launch.

That script offers some experimental gamescope-related features, 
use `~/.local/bin/firefox-kiosk-script --help` to see what's available.

### Recommended extensions

Firefox desktop entries all run under the same profile, so any customization
is shared (extensions, configuration, pinned tabs...), here is a list of
extensions found to be useful:

* General
  * [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
  * [UltraWideo](https://addons.mozilla.org/en-US/firefox/addon/ultrawideo/)
* YouTube
  * [BlockTube](https://addons.mozilla.org/en-US/firefox/addon/blocktube/) 
  * [Enhancer for YouTube](https://addons.mozilla.org/en-US/firefox/addon/enhancer-for-youtube/)
  * [SponsorBlock for YouTube](https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/)
  * (hardware without AV1 support) [h264ify](https://addons.mozilla.org/en-US/firefox/addon/h264ify/)
* Twitch
  * [Unwanted Twitch](https://addons.mozilla.org/en-US/firefox/addon/unwanted-twitch/)


### A note on DRM and Firefox

Most streaming sites apply restrictive anticonsumer technologies to
deliberately worsen the user experience outside proprietary operative
systems.

Those are:

- **Widevine**: proprietary binary extension, Firefox will ask the user before
  installing if required and supported.
- **Intel HDCP**: The only available HDCP implementation for linux, not working
  outside ChromeOS so far.

## Autostart scripts

Those scripts generate autostart entries to automatize startup operations.

* [disable-unredirect.sh](./disable-unredirect.sh)

## System scripts

Those scripts generate desktop entries to automatize common operations,
requiring system-wide changes during installation.

* [reboot-to-hasefroch.sh](./reboot-to-hasefroch.sh)
* [pacmeld.sh](./pacmeld.sh)

This scripts **will perform sudo during its installation** to register
system-wide commands or permissions (via polkit profiles).

## Development

Copy an existing script, such as [firefox-telegram.sh](./firefox-telegram.sh),
search for occurrences of the original service name and replace with the new
one:

* Update comments.
* Inline the SVG icon code into the `iconFile` variable.
* Edit the desktop entry code on the `desktopFile` variable.
* Customize `FIREFOX_KIOSK_APP` env passed to `firefox-kiosk.sh`.

**Feel free to contribute with any script**, just fill a pull request with both
script and updated `README.md` (if not already covered).

## License

WTFPL (see [LICENSE](./LICENSE)).
