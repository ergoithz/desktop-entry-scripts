#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Disney+.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Disney+.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient846" cx="-240.42" cy="85.715" r="174" gradientTransform="matrix(0 -1.6118 .62885 0 168.71 -249.64)" gradientUnits="userSpaceOnUse">
<stop stop-color="#f5feff" offset="0"/>
<stop stop-color="#deffff" offset=".04284"/>
<stop stop-color="#58fffd" offset=".18665"/>
<stop stop-color="#095dc2" offset=".51929"/>
<stop stop-color="#103798" offset=".70326"/>
<stop stop-color="#103798" offset=".82493"/>
<stop stop-color="#1c2b68" stop-opacity="0" offset="1"/>
</radialGradient>
<radialGradient id="radialGradient4615" cx="-166.26" cy="-6.9101" r="83.209" gradientTransform="matrix(3.4338,0,-9.7874e-8,3.4283,702.21,279.96)" gradientUnits="userSpaceOnUse">
<stop stop-color="#1846a6" offset="0"/>
<stop stop-color="#162861" offset=".7"/>
<stop stop-color="#0a1024" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.06c-.0128 30.638-5.2449 76.859-26.918 98.514-21.673 21.655-67.899 26.849-98.536 26.836-30.638-.0128-76.859-5.2449-98.514-26.918s-26.849-67.899-26.836-98.536c.0128-30.638 5.2449-76.859 26.918-98.514s67.899-26.849 98.536-26.836c30.638.0128 76.859 5.2449 98.514 26.918s26.849 67.899 26.836 98.536z" fill="url(#radialGradient4615)"/>
<path d="m168.71 77.17a55.828 55.828 0 00-55.828 55.828 55.828 55.828 0 0055.828 55.828 55.828 55.828 0 0055.828-55.828 55.828 55.828 0 00-55.828-55.828zm0 6.6443a49.183 49.183 0 0149.183 49.183 49.183 49.183 0 01-49.183 49.183 49.183 49.183 0 01-49.183-49.183 49.183 49.183 0 0149.183-49.183z" fill="url(#radialGradient846)"/>
<path d="m54.788 72.53c-8.1238.15429-19.091 1.2927-23.906 6.1245-3.7508 3.764-2.4662 6.6669 1.2172 7.468 8.0728 1.7556 10.985-2.6934 2.1916-3.4094 1.5422-2.6786 20.172-3.0724 28.41-1.7854 34.41 5.3758 56.859 25.836 60.635 30.844 23.081 30.618-12.118 44.278-41.154 38.232l-.16261-25.163c17.6.78431 32.643 3.6955 23.215 8.2791-5.1239 2.491-1.564 3.6987.97367 4.1395 7.0254 1.2204 12.022-5.4972 9.4159-11.363-4.651-10.469-24.485-11.087-33.442-10.634.026-7.3598-.33855-14.19-3.0849-17.695-5.0859-6.1244-6.3296 6.3909-6.8182 18.345-14.277 2.6314-26.551 7.2243-26.624 14.529-.0729 7.3048 11.369 17.392 25.894 24.189.19137 2.3269 1.2826 8.2561 3.0033 9.1717 3.5879 1.9094 8.496-.0263 7.5489-5.4384 5.9776.88782 10.049 2.0647 20.861 1.5424 17.504-.39998 26.165-5.3835 32.468-13.149 6.303-7.7658 9.4354-21.694 1.2989-33.605-18.995-27.806-56.422-41.107-81.941-40.622zm17.504 52.972-1.1482 21.352c-7.5117-4.1437-22.336-11.052-22.04-15.153.30538-4.2215 15.011-4.7672 23.188-6.1992z" fill="#fff"/>
<path d="m165.22 135.55-11.225.18204c-3.5009.0568-2.6521-7.4874 1.079-7.4874h9.984l.16234-11.202c.0741-5.1139 7.6599-1.5799 7.6538 1.0077l-.0238 10.032 9.9028.24351c4.4359.10908 4.4298 7.4125.97405 7.3865l-10.796-.0812-.0812 9.1723c-.0536 6.0615-7.6129 4.6089-7.6161 1.9899z" fill="#fff"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Disney+
Comment=Stream movies and shows
Keywords=streaming;
Exec={{kiosk}} 'https://www.disneyplus.com'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=disneyplus \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
