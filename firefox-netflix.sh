#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Netflix.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Netflix.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient4615-3" cx="-165.34" cy="-7.2892" r="83.21" gradientTransform="matrix(3.434 0 -9.7871e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#232323" offset="0"/>
<stop stop-color="#161616" offset=".7"/>
<stop stop-color="#232323" offset="1"/>
</radialGradient>
<filter id="filter1459" x="-.26101" y="-.13534" width="1.522" height="1.2707" color-interpolation-filters="sRGB">
<feGaussianBlur stdDeviation="5.364897"/>
</filter>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.2449 76.861-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.861-5.2449-98.51-26.92-21.65-21.675-26.85-67.9-26.84-98.54.0128-30.64 5.2449-76.861 26.92-98.51 21.675-21.65 67.9-26.85 98.54-26.84 30.64.012801 76.861 5.2449 98.51 26.92 21.65 21.675 26.85 67.9 26.84 98.54z" fill="url(#radialGradient4615-3)" stroke-width=".26458"/>
<g transform="matrix(1.4562 0 0 1.476 -204.63 -92.497)" stroke-width=".68208">
<path d="m219.22 193.17v-52.459l19.052 50.341c5.6307-.6131 10.528-1.191 16.184-1.7336v-89.4h-14.094v54.337l-19.76-54.337h-15.476v95.137h.0916" fill="#300" filter="url(#filter1459)" opacity=".5"/>
<path d="m219.22 196.7v-52.459l19.052 50.341 16.184-1.7336v-89.4l-14.094 54.337-35.236-54.337v95.137h.0916" fill="#f00" opacity=".1"/>
<path d="m219.22 193.17v-52.459l19.052 50.341c5.6307-.6131 10.528-1.191 16.184-1.7336v-89.4h-14.094v54.337l-19.76-54.337h-15.476v95.137h.0916" fill="#d52a31"/>
</g>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Netflix
Comment=Watch Movies and TV Shows instantly
Exec={{kiosk}} 'http://www.netflix.com/browse'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=netflix \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
