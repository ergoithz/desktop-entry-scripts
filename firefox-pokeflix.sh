#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Pokéflix.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Pokéflix.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg version="1.1" viewBox="0 0 256 256" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#181818" offset="0"/>
<stop stop-color="#222" offset=".7"/>
<stop stop-color="#222" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#a)" stroke-width=".2646"/>
<path d="m131.7 44.22-21.24 36.56h16.74l-7.727 14.3c-12.64 1.312-24.85 5.544-34.95 12.72-5.307-24.16-15-45.73-45.95-54.8.2577 36.46 11.8 58.24 30.78 70.35-6.643 10.01-10.53 22.64-10.24 37.92 1.252 66.01 139.7 65.15 139.9 0 .04647-14.95-4.009-27.4-10.76-37.34 19.53-11.99 31.44-33.89 31.7-70.93-31.45 9.21-40.94 31.33-46.2 55.96-9.849-7.258-21.73-11.76-34.06-13.5l21.81-21.62h-17.36l23.89-29.62h-36.35z" fill="#ffd400"/>
<path d="m126 56.11-13.76 23.2 15.38.8086-8.094 16.19 15.65.8105 12.27-10.93h-13.22l23.33-30.08h-31.56zm-74.08 29.84c-5.9-.1059-12.08 2.703 1.934 19.95 23.46 28.87 30.13 18.72 30.13 18.72l-25.96-37.75s-3.015-.8547-6.105-.9102zm156.3 0c-3.091.05547-6.105.9102-6.105.9102l-25.96 37.75s6.679 10.15 30.13-18.72c14.01-17.24 7.834-20.05 1.934-19.95zm-149.1 65.66c-12.41 79.43 152.2 75.14 139.9 1.041-36.69 47.16-101.9 46.1-139.9-1.041z" fill="#ffb000"/>
<path d="m117.6 98.56 9.597-17.77h-16.74l21.24-36.56h36.35l-23.89 29.62h17.36l-22.67 22.47z" fill="none" stroke="#33363a" stroke-linecap="round" stroke-linejoin="round" stroke-width="4"/>
<path d="m128.6 94.64c-15.21-.08543-30.92 6.075-43.41 15.15l.8633 3.564c26.98-8.936 59.34-8.58 86.04 1.242l1.18-5.178c-10.82-10.98-28.93-14.69-44.68-14.78zm.1211 42.54c-5.645.0585-10.89 1.444-15.92 3.74l16.46 7.984 17.8-7.5c-6.647-3.014-12.69-4.283-18.34-4.225z" fill="#fff800"/>
<path d="m38.61 53.01c.09916 14.02 1.873 25.86 5.09 35.83 12.1 3.518 25.88-4.038 18.73-23.52-6.119-5.173-13.86-9.389-23.82-12.3zm181.4 0c-9.956 2.916-17.7 7.131-23.82 12.3-7.154 19.48 6.634 27.04 18.73 23.52 3.217-9.966 4.989-21.81 5.088-35.83zm-124.6 71.28a12.46 12.46 0 00-12.46 12.46 12.46 12.46 0 0012.46 12.46 12.46 12.46 0 0012.46-12.46 12.46 12.46 0 00-12.46-12.46zm69.22 0a12.46 12.46 0 00-12.46 12.46 12.46 12.46 0 0012.46 12.46 12.46 12.46 0 0012.46-12.46 12.46 12.46 0 00-12.46-12.46zm-42.17 21.22 7.221 10.69 7.363-10.69h-14.58z" fill="#33363a" style="paint-order:fill markers stroke"/>
<path d="m85.57 152.9a9.393 9.396 0 00-9.393 9.396 9.393 9.396 0 009.393 9.395 9.393 9.396 0 009.393-9.395 9.393 9.396 0 00-9.393-9.396zm89.03 0a9.393 9.396 0 00-9.393 9.396 9.393 9.396 0 009.393 9.395 9.393 9.396 0 009.393-9.395 9.393 9.396 0 00-9.393-9.396z" fill="#ff1a1a" style="paint-order:fill markers stroke"/>
<path d="m115.8 153.9c1.912 9.087 12.8 7.794 13.72.1445 1.227 7.681 12.66 9.192 13.72-.4333" fill="none" stroke="#33363a" stroke-linecap="round" stroke-linejoin="round" stroke-width="4"/>
<path d="m90.5 130.2a3.366 3.367 0 00-3.365 3.367 3.366 3.367 0 003.365 3.365 3.366 3.367 0 003.367-3.365 3.366 3.367 0 00-3.367-3.367zm69.37 0a3.366 3.367 0 00-3.365 3.367 3.366 3.367 0 003.365 3.365 3.366 3.367 0 003.367-3.365 3.366 3.367 0 00-3.367-3.367z" fill="#fff" style="paint-order:fill markers stroke"/>
<path d="m86.64 139.6c1.305 3.638 4.82 6.509 8.729 6.51 3.909.00025 7.425-2.872 8.73-6.51zm69.22 0c1.305 3.638 4.821 6.51 8.73 6.51 3.909.00025 7.425-2.872 8.73-6.51z" fill="#6d5c4d" style="paint-order:fill markers stroke"/>
<path d="m76.3 163.2a9.393 9.396 0 009.271 8.475 9.393 9.396 0 009.271-8.475h-18.54zm89.03 0a9.393 9.396 0 009.271 8.475 9.393 9.396 0 009.271-8.475h-18.54z" fill="#d61d1d" style="paint-order:fill markers stroke"/>
<path d="m174.5 108.5c-12.97-9.4-29.36-13.99-45.74-13.83-15.85.152-31.67 4.757-44.38 13.72m-14.72 14.42c-7.305 10.03-11.86 22.71-12.13 38.03-1.136 63.4 144.5 67.4 142.9-.5391-.3584-15.14-4.715-27.62-11.7-37.49" fill="none" stroke="#33363a" stroke-linejoin="round" stroke-width="4"/>
<path d="m85.37 111.6c-5.149-25.68-14.18-49.08-46.76-58.62.2803 39.66 13.9 61.96 35.94 73.33m109.5 0c22.04-11.37 35.66-33.67 35.94-73.33-32.58 9.542-41.61 32.94-46.76 58.62" fill="none" stroke="#33363a" stroke-linecap="round" stroke-linejoin="round" stroke-width="4"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Pokéflix
Comment=Watch Pokémon episodes and movies
Keywords=streaming;
Exec={{kiosk}} 'https://www.pokeflix.tv/index/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

read -d '' scriptDesktopFile << EOF ||:
${desktopFile}
Actions=gamescope-480p;

[Desktop Action gamescope-480p]
Name=Upscale from 480p
Exec={{kiosk}} --480p 'https://www.pokeflix.tv/index/'
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=pokeflix \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  FIREFOX_KIOSK_SCRIPT_DESKTOP="$scriptDesktopFile" \
  bash
