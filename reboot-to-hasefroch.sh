#!/bin/bash -e
: << DESCRIPTION

This script generates a desktop shortcut to reboot into Windows on UEFI systems
adding a command and a sudoers entry for group users and the current host.

This is tested with Arch-like distros and efibootmgr must be installed.

License: MIT

DESCRIPTION

# directory definitions
binDir="/usr/bin"
polkitDir="/usr/share/polkit-1/actions"
applicationsDir="/usr/share/applications"

# file definitions
read -d '' desktopFile << EOF ||:
[Desktop Entry]
Name=Reboot to Hasefroch
Comment=Reboot to OS whose name shall not be said
Exec=${binDir}/reboot-to-hasefroch
Terminal=false
Icon=system-reboot
StartupNotify=false
Type=Application
Categories=System;
EOF

read -d '' scriptFile << 'EOF' ||:
#!/bin/sh -e

NUM=$(efibootmgr | grep Windows | tail -n1 | cut -c 5-8)

if [ -z "$NUM" ]; then
  >&2 echo 'Hasefroch EFI entry not found, check efibootmgr output manually.'
  exit 1
fi

if [ "$UID" != "0" ]; then
    pkexec $0 $*
    exit $?
fi

efibootmgr --bootnext $NUM
systemctl reboot
EOF

read -d '' polkitFile << EOF ||:
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
 "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/PolicyKit/1.0/policyconfig.dtd">
<policyconfig>
  <vendor>reboot-to-hasefroch</vendor>
  <vendor_url>reboot-to-hasefroch</vendor_url>
  <icon_name>system-reboot</icon_name>
  <action id="org.freedesktop.policykit.pkexec.reboot-to-hasefroch">
   <description>Run "reboot-to-hasefroch"</description>
   <message>Authentication is required to run reboot-to-hasefroch</message>
   <defaults>
     <allow_any>auth_admin</allow_any>
     <allow_inactive>auth_admin</allow_inactive>
     <allow_active>auth_admin</allow_active>
   </defaults>
     <annotate key="org.freedesktop.policykit.exec.path">$binDir/reboot-to-hasefroch</annotate>
   </action>
</policyconfig>
EOF

# create basic files
echo 'System-wide installation required due PolicyKit support.'
sudo bash << EOF
echo '$desktopFile' > '$applicationsDir/reboot-to-hasefroch.desktop'
chmod +x '$applicationsDir/reboot-to-hasefroch.desktop'
echo '$scriptFile' > '$binDir/reboot-to-hasefroch'
chmod +x '$binDir/reboot-to-hasefroch'
echo '$polkitFile' > '$polkitDir/com.ergoithz.reboot-to-hasefroch.policy'
EOF
echo 'Installed.'
