#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for web apps.

Firefox profile features:
* Tabs and menubar will autohide.
* Homepage set to blank.

This script is meant to be used by other firefox profile scripts,
but can be called directly to generate 'blank page' kiosk shortcut.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#3a295b" offset="0"/>
<stop stop-color="#342552" offset=".7"/>
<stop stop-color="#3a295b" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#a)" stroke-width=".2646"/>
<path d="m154.2 44.75c-6.94 6.648-13.13 23.79-18.17 43.54 19.88 4.044 34.59 21.53 34.17 41.82-.8659 42.01 1.637 31.18 26.67-5.334-.2454-42.68-37.34-53.35-42.68-80.02z" fill="#f0dc51"/>
<path d="m154.2 44.75c-13.87 4.624-27.74 21.28-31.19 43 8.248-1.031 20 .4577 30.48 8.502-15.48-17.04-11.92-32.43.7031-51.5z" fill="#efca49"/>
<path d="m100.9 76.75s-13.43 5.335-18.67 16l18.67 5.335z" fill="#f7a166"/>
<path d="m84.89 130.1 5.335 10.67c6.417-8.268 13.34-10.67 24.01-10.67 5.335 0 10.87-5.335 13.34-10.67-.9768-.593-2.472-1.234-4.324-2.021zm58.68-21.34c9.933 6.378 16 17.54 16 29.34l9.899-16.09c-5.816-8.02-15.24-13.25-25.9-13.25z" fill="#241c35"/>
<path d="m143.6 103.4c9.933 6.378 16 17.54 16 29.34.004 23.42-21.28 35.17-34.02 34.58-16.89-.7722-35.33-12.57-35.33-31.92 6.417-8.268 13.34-10.67 24.01-10.67 5.335 0 10.87-5.335 13.34-10.67-14.9-3.346-44.42-28.95-60.98-.2664-24.03 41.63 109 108.3 109 21.6 0-17.68-14.33-32.01-32.01-32.01z" fill="#eec750"/>
<path d="m79.55 71.42c-21.74 12.76-30 42.6-10.67 42.68 1e-6 0 5.334-5.334 16-5.334 10.67 0 5.335 5.334 42.68 5.334-3.907-2.372-16.03-5.449-26.67-16-5.358-5.312-10.67-5.336-18.67-5.336-3.149-7.263-2.666-21.34-2.666-21.34zm117.4 32.01c0 35.36-31.05 64.02-69.35 64.02v37.34c55.5-4.262 77.07-50.61 69.35-101.4z" fill="#f3974c"/>
<path d="m79.55 71.42c-18.41 10.67-32.11 33.5-32.01 58.68.1106 27.34 25.11 69.17 80.02 69.35 34.13.1146 58.68-40.01 58.68-50.68-13.41 16.9-52.86 19.81-63.4 18.4-7.652-.7109-37.96-5.835-37.96-42.4-15.12 0-36.37-32.66-5.334-53.35z" fill="#f37052"/>
<path d="m175.6 66.08c16 16 21.34 42.68 21.34 64.02 0 37.34-32.01 69.35-72.02 69.35 49.67-.7559 82.69-25.16 82.69-69.35-.00074-25.99-10.78-49.02-32.01-64.02z" fill="#f3b150"/>
<path d="m47.54 130.1c0 44.19 35.83 80.02 80.02 80.02 44.19 0 80.02-35.83 80.02-80.02 0 56.6-58.64 69.45-80.08 69.27-44.83-.3797-79.97-41.85-79.97-69.27z" fill="#e33e5f"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Firefox (Kiosk Mode)
Comment=Run Firefox in kiosk mode
Exec={{kiosk}}
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

read -d '' userChrome << 'EOF' ||:
#navigator-toolbox:not(:hover):not(:focus-within):not(:has([open=true])) {
  position: absolute !important;
  z-index: var(--browser-area-z-index-toolbox-while-animating, 2) !important;
  left: 0;
  right: 0;
  bottom: 100%;
  transform: translateY(.5em);
  opacity: 0;
}
EOF

read -d '' userPrefs << 'EOF' ||:

// Disable crap
user_pref("browser.startup.homepage", "about:newtab");
user_pref("browser.startup.homepage_override.mstone", "ignore");
user_pref("browser.tabs.firefox-view", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("media.videocontrols.picture-in-picture.video-toggle.enabled", false);
user_pref("extensions.pocket.enabled", false);
user_pref("browser.newtabpage.activity-stream.telemetry", false);
user_pref("widget.use-xdg-desktop-portal.file-picker", 1);

// Cleaner newtab
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", true);

// Streaming sites
user_pref("media.eme.enabled", true);
user_pref("media.eme.encrypted-media-encryption-scheme.enabled", true);
user_pref("media.eme.hdcp-policy-check.enabled", true);
user_pref("media.autoplay.default", 0);  // allowed

// XUL hacking
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("layout.css.has-selector.enabled", true);

// Gamescope support (firefox-kiosk-script)
user_pref("browser.chrome.toolbar_tips", true); // val:desktop
user_pref("full-screen-api.ignore-widgets", false); // val:gamescope
user_pref("browser.sessionstore.resume_from_crash", true); // val:desktop
user_pref("layout.css.devPixelsPerPx", "-1"); // val:scaling

// Video acceleration
// https://wiki.archlinux.org/title/firefox#Hardware_video_acceleration
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.prefer-non-ffvpx", true);
user_pref("media.navigator.mediadatadecoder_vpx_enabled", true);
user_pref("gfx.x11-egl.force-enabled", true);
user_pref("gfx.x11-egl.force-disabled", false);
// not:vp9 user_pref("media.mediasource.vp9.enabled", false);
// not:av1 user_pref("media.av1.enabled", false);
EOF

read -d '' kioskScript << 'EOF' ||:
#!/bin/sh -e

internal=''

while :; do
  case "$1" in
    -h|--help)
      cat << EOH
Usage: $0 [ OPTIONS ... ] [ -- FIREFOX_OPTIONS ... ]

firefox-kiosk-script options
  --480p        Run Firefox inside gamescope (640x480 FSR).
  --720p        Run Firefox inside gamescope (1280x720 FSR).
  --1080p       Run Firefox inside gamescope (1920x1080 FSR).
  -h or --help  Print this message.

firefox help:
EOH
      exec {{firefox}} --help
      exit
      ;;
    --360p)
      internal=540x360
      ;;
    --480p)
      internal=640x480
      ;;
    --720p)
      internal=1280x720
      ;;
    --1080p)
      internal=1920x1080
      ;;
    --)
      shift
      break
      ;;
    *)
      break
      ;;
  esac
  shift
done

envs=''
opts='--new-window'
size="$(xdpyinfo | awk '/dimensions/ {print $2}')"
gamescope=true/false
desktop=false/true
scaling=1/-1

if [ "$XDG_CURRENT_DESKTOP" = 'gamescope' ]; then
  envs='env GTK_CSD=0 MOZ_ENABLE_WAYLAND=0'
  opts="--no-remote --width=${size%x*} --height=${size#*x}"
  gamescope=false/true
  desktop=true/false
  scaling=-1/1
elif [ -n "$internal" ]; then
  set -x
  exec gamescope \\
    --output-width=${size%x*} \\
    --output-height=${size#*x} \\
    --nested-width=${internal%x*} \\
    --nested-height=${internal#*x} \\
    --filter=fsr \\
    --sharpness=0 \\
    -- $0 $@
fi

sed -i \\
  -e"s|\\(,[ ]*\\)${gamescope%/*}\\(); // val:gamescope\\b\\)|\\1${gamescope#*/}\\2|g" \\
  -e"s|\\(,[ ]*\\)${desktop%/*}\\(); // val:desktop\\b\\)|\\1${desktop#*/}\\2|g" \\
  -e"s|\\(,[ ]*"'"'"\\)${scaling%/*}\\("'"'"); // val:scaling\\b\\)|\\1${scaling#*/}\\2|g" \\
  "${HOME}/{{profile}}/user.js"

set -x
exec $envs {{firefox}} $opts --profile "${HOME}/{{profile}}" $@
EOF

firefox=firefox
profile='.mozilla/firefox/firefox-kiosk'
if flatpak info org.mozilla.firefox &> /dev/null; then
  firefox='flatpak run org.mozilla.firefox'
  profile=".var/app/org.mozilla.firefox/${profile}"
elif [ -f /snap/bin/firefox ]; then
  profile="snap/firefox/common/${profile}"
fi

vaout=$(vainfo | grep VAEntrypointVLD || :)
if [ -z "$vaout" ]; then
  >&2 echo -e '\e[31m[WARNING] vainfo (libva-utils) not found, hwaccel detection disabled\e[0m'
else
  if ! echo "$vaout" | grep 'VAProfileVP9Profile' > /dev/null; then
    >&2 echo -e '\e[36m[INFO] No VP9 hardware acceleration\e[0m'
    userPrefs=${userPrefs//'// not:vp9 '/''}
  fi
  if ! echo "$vaout" | grep 'VAProfileAV1Profile' > /dev/null; then
    >&2 echo -e '\e[36m[INFO] No AV1 hardware acceleration\e[0m'
    userPrefs=${userPrefs//'// not:av1 '/''}
  fi
fi

FIREFOX_KIOSK_USER_CHROME="${FIREFOX_KIOSK_USER_CHROME:-$userChrome}"
FIREFOX_KIOSK_USER_PREFS="${FIREFOX_KIOSK_USER_PREFS:-$userPrefs}"
FIREFOX_KIOSK_ICON="${FIREFOX_KIOSK_ICON:-$iconFile}"
FIREFOX_KIOSK_DESKTOP="${FIREFOX_KIOSK_DESKTOP:-$desktopFile}"
FIREFOX_KIOSK_APP="${FIREFOX_KIOSK_APP:-kiosk}"

desktopName="firefox-${FIREFOX_KIOSK_APP}"
binDir="${HOME}/.local/bin"
profileDir="${HOME}/${profile}"
chromeDir="${HOME}/${profile}/chrome"
iconsDir="${HOME}/.local/share/icons"
applicationsDir="${HOME}/.local/share/applications"
execKiosk="${firefox} --profile ${profileDir} --new-window"

if command -v gamescope &> /dev/null; then
  >&2 echo -e '\e[36m[INFO] gamescope detected, using firefox-kiosk-script\e[0m'
  FIREFOX_KIOSK_DESKTOP="${FIREFOX_KIOSK_SCRIPT_DESKTOP:-$FIREFOX_KIOSK_DESKTOP}"
  execKiosk="${binDir}/firefox-kiosk-script"
fi

FIREFOX_KIOSK_SCRIPT=${kioskScript}
FIREFOX_KIOSK_SCRIPT=${FIREFOX_KIOSK_SCRIPT//'{{firefox}}'/"${firefox}"}
FIREFOX_KIOSK_SCRIPT=${FIREFOX_KIOSK_SCRIPT//'{{profile}}'/"${profile}"}

FIREFOX_KIOSK_DESKTOP=${FIREFOX_KIOSK_DESKTOP//'{{kiosk}}'/"${execKiosk}"}
FIREFOX_KIOSK_DESKTOP=${FIREFOX_KIOSK_DESKTOP//'{{icon}}'/"${desktopName}"}

mkdir -p "$binDir" "$profileDir" "$chromeDir" "$iconsDir" "$applicationsDir"
echo "$FIREFOX_KIOSK_SCRIPT" > "${binDir}/firefox-kiosk-script"
echo "$FIREFOX_KIOSK_USER_PREFS" > "${profileDir}/user.js"
echo "$FIREFOX_KIOSK_USER_CHROME" > "${chromeDir}/userChrome.css"
echo "$FIREFOX_KIOSK_ICON" > "${iconsDir}/${desktopName}.svg"
echo "$FIREFOX_KIOSK_DESKTOP" > "${applicationsDir}/${desktopName}.desktop"
chmod +x "${binDir}/firefox-kiosk-script" "${applicationsDir}/${desktopName}.desktop"

command -v update-desktop-database &> /dev/null && update-desktop-database "$applicationsDir" ||:
command -v xdg-desktop-menu &> /dev/null && xdg-desktop-menu forceupdate ||:

echo "Shortcut for '${FIREFOX_KIOSK_APP}' created."
