#!/bin/bash -e
: << DESCRIPTION

This script generates a desktop shortcut to an script which iteratively
runs meld (a diff/merge software) against .pacnew files.

This is tested with Arch-like distros and both meld and zenity must be
already installed.

License: MIT

DESCRIPTION

# directory definitions
binDir="/usr/bin"
polkitDir="/usr/share/polkit-1/actions"
applicationsDir="/usr/share/applications"

# file definitions
read -d '' desktopFile << EOF ||:
[Desktop Entry]
Comment=Merge pacnew configuration files
Name=Pacmeld
GenericName=Merge pacnew
Exec=${binDir}/pacmeld
Icon=org.gnome.Meld
StartupNotify=false
Categories=System;
Type=Application
EOF

read -d '' scriptFile << 'EOF' ||:
#!/bin/sh

name=$(basename "$0")
pacnew=$(2>/dev/null find /etc -type f -name "*.pacnew")
pacnew_re="s|\\(\\.pacnew\\)\\+\\$||g"

if [ -z "${pacnew}" ]; then
    exec zenity \\
        --info \\
        --title "${name} - info" \\
        --icon-name org.gnome.Meld \\
        --text "<big><b>Nothing to do</b></big>\\nNo <b>.pacnew</b> files found under <b>/etc</b>." \\
        --no-wrap
fi

if [ "$UID" != "0" ]; then
    exec pkexec $0 $*
fi

echo "${pacnew}" \\
| sed "${pacnew_re}" \\
| zenity \\
    --list \\
    --title "${name} - conflicts" \\
    --text "<big><b>Conflicts</b></big>\\nThe following files got new versions:" \\
    --column=path \\
    --hide-header \\
    || exit 1

for config in ${pacnew}; do
    original=$(
        echo "${config}" \\
        | sed "${pacnew_re}"
        )
    meld "${original}" "${config}"
done

removal=$(
    echo "${pacnew}" \\
    | sed  "s|.*|\\0\\n\\0|" \\
    | zenity \\
        --list \\
        --title "${name} - cleanup" \\
        --text "<big><b>Final cleanup</b></big>\\nCheck which files you want to remove:" \\
        --separator " " \\
        --checklist \\
        --column=remove \\
        --column=path \\
        --hide-header \\
        --multiple
    )

if [ ! -z "$removal" ]; then
    rm -f ${removal}
fi
EOF

read -d '' polkitFile << EOF ||:
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
 "-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/PolicyKit/1.0/policyconfig.dtd">
<policyconfig>
  <vendor>pacmeld</vendor>
  <vendor_url>pacmeld</vendor_url>
  <icon_name>meld</icon_name>
  <action id="org.freedesktop.policykit.pkexec.pacmeld">
   <description>Run "pacmeld"</description>
   <message>Authentication is required to run pacmeld</message>
   <defaults>
     <allow_any>auth_admin</allow_any>
     <allow_inactive>auth_admin</allow_inactive>
     <allow_active>auth_admin</allow_active>
   </defaults>
     <annotate key="org.freedesktop.policykit.exec.path">$binDir/pacmeld</annotate>
     <annotate key="org.freedesktop.policykit.exec.allow_gui">true</annotate>
   </action>
</policyconfig>
EOF

echo 'System-wide installation required due PolicyKit support.'
sudo bash << EOF
echo '$desktopFile' > '$applicationsDir/pacmeld.desktop'
chmod +x '$applicationsDir/pacmeld.desktop'
echo '$scriptFile' > '$binDir/pacmeld'
chmod +x '$binDir/pacmeld'
echo '$polkitFile' > '$polkitDir/com.ergoithz.pacmeld.policy'
EOF
echo 'Installed.'
