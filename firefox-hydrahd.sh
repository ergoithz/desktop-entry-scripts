#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for HydraHD.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open HydraHD.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse"><stop stop-color="#202020" offset="0"/>
<stop offset=".7"/>
<stop stop-color="#2c2c2c" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#a)" stroke-width=".2646"/>
<path d="m104.7 57.14c-.2765-.03376-.4024.2337-.4934.4305-.804 1.738-1.073 6.644-2.885 15.98-4.169 21.48-14.47 47.4-17.07 49.8-1.214 1.121-6.029.4438-9.499 2.969-1.577 2.313 6.303 5.553 6.303 7.305-5.789 17.23-13.02 33.97-19.99 50.76 1.911 2.724 3.688 5.555 5.963 7.997 3.117 3.182 10.49 6.857 11.69 6.428.9182-.6966 1.01-1.99 1.28-3.068 5.06-20.33 14.38-55.52 19.03-58.74 3.508-2.428 42.27-5.124 42.59-4.809 1.048 1.048-10.77 36.37-6.625 51.58 1.735 6.37 11.16 14.83 13.27 15.09 1.463.1805 1.798-1.73 1.798-4.361 0-16.17 11.05-60.62 14.07-62.2 1.506-.7901 5.275.5709 6.626-2.704 1.753-4.248-2.598-8.436-2.239-12.55.3831-4.394 10.58-29.54 19.89-41.46l3.033-3.885c-4.189-3.766-9.186-6.404-14.46-8.323-2.29-.7873-4.126-1.138-4.761-.738-3.824 2.412-11.94 20.73-24.66 53.29-12.45.2398-24.96 1.666-37.25 3.66-2.411.4454-3.25.2366-3.25-.8066 5.277-15.24 12.8-29.65 20.12-44.01-2.242-2.878-14.61-18.26-22.47-17.64z" fill="#ce0d0d"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=HydraHD
Comment=Watch Movies and Series for free. Biggest library
Keywords=movie;series;streaming;
Exec={{kiosk}} 'https://hydrahd.me/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=hydrahd \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
