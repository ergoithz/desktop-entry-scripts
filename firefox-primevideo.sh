#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Prime Video.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Prime Video.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient4615-3" cx="-165.34" cy="-7.2892" r="83.21" gradientTransform="matrix(3.434 0 -9.7871e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#7dad3f" offset="0"/>
<stop stop-color="#7dad3f" offset=".7"/>
<stop stop-color="#73a03a" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.2449 76.861-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.861-5.2449-98.51-26.92-21.65-21.675-26.85-67.9-26.84-98.54.0128-30.64 5.2449-76.861 26.92-98.51 21.675-21.65 67.9-26.85 98.54-26.84 30.64.012801 76.861 5.2449 98.51 26.92 21.65 21.675 26.85 67.9 26.84 98.54z" fill="url(#radialGradient4615-3)" stroke-width=".26458"/>
<path d="m82.334 75.956c-2.3596-.11231-4.3203 1.3582-4.3203 5.0039v94.5c0 4.3294 4.4427 5.8172 8.3867 4.0312l102.8-46.57c4.076-1.846 4.082-7.5996 0-9.4336l-104.4-46.92-.0332.006c-.81725-.36725-1.6471-.57975-2.4336-.61719z" fill="#fff"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Prime Video
Comment=Enjoy exclusive Amazon Originals as well as popular movies and TV shows
Keywords=streaming;
Exec={{kiosk}} 'https://www.primevideo.com'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=primevideo \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
