#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Twitch.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Twitch.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-166.3" cy="-6.91" r="83.21" gradientTransform="matrix(3.434,0,-9.787e-8,3.428,702.2,280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#4d4d4d" offset="0"/>
<stop stop-color="#333" offset=".7"/>
<stop stop-color="#1a1a1a" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0128 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.0128-76.86-5.245-98.51-26.92-21.66-21.67-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.67-21.66 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.66 21.67 26.85 67.9 26.84 98.54z" fill="url(#a)"/>
<path class="st1" d="m86.42 50.17-31.71 31.71v114.2h38.05v31.71l31.71-31.71h25.36l57.07-57.07v-88.77z" fill="#9146ff" stroke-width=".9244"/>
<path d="m92.76 62.86v95.12h28.54v22.19l22.19-22.19h25.36l25.36-25.36v-69.74zm34.88 22.19h12.68v38.05h-12.68zm34.88 0h12.68v38.05h-12.68z" fill="#fff" stroke-width=".9244"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Twitch
Comment=Live streaming platform for gamers and the things we love
Keywords=games;streaming;
Exec={{kiosk}} 'https://www.twitch.tv/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=twitch \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
