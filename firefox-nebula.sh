#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Nebula.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Nebula.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient4615" cx="-165.34" cy="-7.2892" r="83.21" gradientTransform="matrix(3.4341 0 -9.7873e-8 3.4281 702.21 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#fff" stop-opacity=".97255" offset="0"/>
<stop stop-color="#fff" offset=".7"/>
<stop stop-color="#eeeef9" stop-opacity=".97255" offset="1"/>
</radialGradient>
<radialGradient id="radialGradient1919" cx="-83.126" cy="32.936" r="17.067" gradientTransform="matrix(1 0 0 .94325 117.17 3.6281)" gradientUnits="userSpaceOnUse">
<stop stop-color="#68baee" offset="0"/>
<stop stop-color="#2132a3" offset="1"/>
</radialGradient>
<radialGradient id="radialGradient21483" cx="-83.653" cy="33.2" r="17.067" gradientTransform="matrix(.71978 .012699 -.012139 .68805 95.177 13.157)" gradientUnits="userSpaceOnUse">
<stop stop-color="#fff" offset="0"/>
<stop stop-color="#2ebcf2" offset=".4655"/>
<stop stop-color="#5fcfdd" offset="1"/>
</radialGradient>
<filter id="filter52528" x="-.085363" y="-.14698" width="1.1707" height="1.294" color-interpolation-filters="sRGB">
<feGaussianBlur stdDeviation="0.88407979"/>
</filter>
<filter id="filter53935" x="-.1285" y="-.12011" width="1.257" height="1.2402" color-interpolation-filters="sRGB">
<feGaussianBlur stdDeviation="1.1595956"/>
</filter>
<linearGradient id="linearGradient62985" x1="-91.62" x2="-69.872" y1="26.167" y2="35.469" gradientTransform="translate(117.17,1.7589)" gradientUnits="userSpaceOnUse">
<stop stop-color="#c52ac0" stop-opacity=".93973" offset="0"/>
<stop stop-color="#931f8f" stop-opacity=".66546" offset="1"/>
</linearGradient>
<clipPath id="clipPath929">
<path d="m34.044 50.793-3.9027-12.47-13.164.1723 10.752-7.5351-4.2331-12.364 10.548 7.8138 10.548-7.8138-4.2331 12.364 10.752 7.5351-13.164-.17229z" stop-color="#000000" stroke-width=".37659" style="-inkscape-stroke:none"/>
</clipPath>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.862-26.921 98.511-21.67 21.661-67.901 26.851-98.542 26.84-30.64-.01286-76.862-5.245-98.511-26.921-21.65-21.675-26.851-67.901-26.84-98.542.0128-30.64 5.245-76.862 26.921-98.511 21.675-21.65 67.901-26.851 98.542-26.84 30.64.012801 76.862 5.245 98.511 26.921 21.65 21.675 26.851 67.901 26.84 98.542z" fill="url(#radialGradient4615)" stroke-width=".26458"/>
<g transform="matrix(3.7796,0,0,3.7796,2.2947e-5,3.7696e-4)" clip-path="url(#clipPath929)">
<path d="m34.044 50.793-3.9027-12.47-13.164.1723 10.752-7.5351-4.2331-12.364 10.548 7.8138 10.548-7.8138-4.2331 12.364 10.752 7.5351-13.164-.17229z" fill="url(#radialGradient1919)" stop-color="#000000" stroke-width=".099639" style="-inkscape-stroke:none"/>
<path transform="matrix(1.1633,0,0,1.1805,-5.9905,-5.4609)" d="m27.897 36.826-3.6373-2.6348 5.9526-3.6813-2.9358-3.5044-1.7144-3.9642 4.277 2.3868 7.1634 5.5191 8.1211 4.0609 3.9922 2.4695-4.2746-.72273-1.7492.67664z" fill="url(#linearGradient62985)" filter="url(#filter52528)" stop-color="#000000" stroke-width=".085026" style="-inkscape-stroke:none;mix-blend-mode:normal"/>
<path transform="matrix(1.1094,0,0,1.108,-3.6641,-3.7524)" d="m33.084 46.344-2.8095-8.9772-7.6156.13116 7.7403-5.4244-3.0474-8.9005 7.5934 5.6251 7.5934-5.6251-3.0474 8.9005 4.8262 3.2233-8.4238 2.07z" fill="url(#radialGradient21483)" filter="url(#filter53935)" stop-color="#000000" stroke-width=".089871" style="-inkscape-stroke:none;mix-blend-mode:normal"/>
</g>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Nebula
Comment=Thoughtful content from some of the smartest people online
Keywords=documentary;streaming;
Exec={{kiosk}} 'https://nebula.tv/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=nebula \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
