#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for local Python docs.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open local Python docs path (tested on Arch linux).

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.083 702.2 277.5)" gradientUnits="userSpaceOnUse">
<stop stop-color="#fff4a6" offset="0"/>
<stop stop-color="#fff" offset=".7689"/>
<stop stop-color="#7cb6f1" offset="1"/>
</radialGradient>
<linearGradient id="c" x1="151" x2="112" y1="192.4" y2="137.3" gradientTransform="matrix(.9967 0 0 1.006 2.976 7.713)" gradientUnits="userSpaceOnUse">
<stop stop-color="#ffd43b" offset="0"/>
<stop stop-color="#ffe873" offset="1"/>
</linearGradient>
<linearGradient id="b" x1="26.65" x2="135.7" y1="20.6" y2="114.4" gradientTransform="matrix(.9967 0 0 1.006 2.976 7.713)" gradientUnits="userSpaceOnUse">
<stop stop-color="#5a9fd4" offset="0"/>
<stop stop-color="#306998" offset="1"/>
</linearGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#a)" stroke-width=".2646"/>
<path d="m126.9 28.45c-8.123.03774-15.88.7304-22.7 1.939-20.11 3.553-23.76 10.99-23.76 24.7v18.11h47.51v6.035h-65.35c-13.81 0-25.91 8.3-29.68 24.08-4.363 18.09-4.556 29.4 0 48.29 3.377 14.07 11.44 24.08 25.25 24.08h16.34v-21.71c0-15.68 13.57-29.52 29.68-29.52h47.45c13.21 0 23.76-10.88 23.76-24.13v-45.24c0-12.88-10.86-22.54-23.76-24.7-8.162-1.359-16.63-1.976-24.75-1.939zm-25.69 14.56c4.908 0 8.915 4.074 8.915 9.081-1e-5 4.99-4.008 9.027-8.915 9.027-4.924-2e-6-8.915-4.035-8.915-9.027-1e-6-5.008 3.991-9.081 8.915-9.081z" fill="url(#b)" stroke-width=".9344"/>
<path d="m181.3 79.23v21.1c0 16.36-13.87 30.12-29.68 30.12h-47.45c-13 0-23.76 11.12-23.76 24.13v45.24c0 12.88 11.2 20.45 23.76 24.15 15.04 4.423 29.47 5.222 47.45 0 11.96-3.462 23.76-10.43 23.76-24.15v-18.11h-47.45v-6.035h71.22c13.81 0 18.96-9.631 23.76-24.08 4.96-14.88 4.749-29.2 0-48.29-3.413-13.75-9.93-24.08-23.76-24.08zm-26.69 114.6c4.924 1e-5 8.915 4.035 8.915 9.027 0 5.008-3.991 9.081-8.915 9.081-4.908 0-8.915-4.074-8.915-9.081 1e-5-4.99 4.008-9.027 8.915-9.027z" fill="url(#c)" stroke-width=".9344"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Python Documentation
Comment=Open Python Developer Documentation
Exec={{kiosk}} 'file:///usr/share/doc/python/html/index.html'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=Python;Development;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=python-docs \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
