#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Web Telegram.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Web Telegram.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<radialGradient id="a" cx="128" cy="254.4" r="125.4" gradientTransform="matrix(2.038 0 0 2.028 -132.8 -261.4)" gradientUnits="userSpaceOnUse">
<stop stop-color="#1d93d2" offset="0"/>
<stop stop-color="#34ace1" offset=".7314"/>
<stop stop-color="#34ace1" offset=".9068"/>
<stop stop-color="#33abe0" offset="1"/>
</radialGradient>
</defs>
<path d="m128.1 2.641c-30.64-.01-76.87 5.19-98.54 26.84-21.68 21.65-26.91 67.87-26.92 98.51-.01 30.64 5.19 76.86 26.84 98.54 21.65 21.68 67.87 26.91 98.51 26.92 30.64.01 76.87-5.18 98.54-26.84 21.68-21.65 26.91-67.87 26.92-98.51.01-30.64-5.19-76.86-26.84-98.54-21.65-21.68-67.87-26.91-98.51-26.92z" fill="url(#a)"/>
<path d="m84.5 138.2 16.44 45.52s2.056 4.258 4.258 4.258c2.202 0 34.95-34.07 34.95-34.07l36.41-70.34-91.47 42.87z" fill="#c8daea" stroke-width="1.175"/>
<path d="m106.3 149.9-3.158 33.55s-1.322 10.28 8.953 0c10.28-10.28 20.11-18.21 20.11-18.21" fill="#a9c6d8" stroke-width="1.175"/>
<path d="m84.76 139.8-33.83-11.02s-4.038-1.639-2.741-5.359c.2678-.767.807-1.419 2.423-2.546 7.497-5.224 138.7-52.39 138.7-52.39s3.707-1.248 5.896-.4182c1.001.3795 1.64.8082 2.178 2.374.1962.5697.309 1.781.2936 2.986-.0106.8681-.1175 1.674-.195 2.936-.8 12.9-24.72 109.2-24.72 109.2s-1.431 5.633-6.558 5.824c-1.87.0705-4.139-.309-6.853-2.643-10.06-8.656-44.84-32.02-52.52-37.17-.4335-.289-.5568-.666-.6308-1.033-.108-.5415.4816-1.214.4816-1.214s60.56-53.83 62.16-59.48c.1245-.4382-.3442-.6543-.98-.4652-4.022 1.48-73.75 45.51-81.44 50.37-.4499.2843-1.711.101-1.711.101" fill="#fff" stroke-width="1.175"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Telegram Web
Comment=Official free messaging web app based on Telegram API for speed and security
Keywords=chat;messaging;
Exec={{kiosk}} 'https://web.telegram.org'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Keyworkds=Internet;Cell;Phone;Chat;
Categories=;Network;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=telegram \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
