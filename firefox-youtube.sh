#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for YouTube.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open YouTube.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="radialGradient4615-3" cx="-165.34" cy="-7.2892" r="83.21" gradientTransform="matrix(3.434 0 -9.7871e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#e03535" offset="0"/>
<stop stop-color="#df2e2e" offset=".7"/>
<stop stop-color="#ce1f1f" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.2449 76.861-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.861-5.2449-98.51-26.92-21.65-21.675-26.85-67.9-26.84-98.54.0128-30.64 5.2449-76.861 26.92-98.51 21.675-21.65 67.9-26.85 98.54-26.84 30.64.012801 76.861 5.2449 98.51 26.92 21.65 21.675 26.85 67.9 26.84 98.54z" fill="url(#radialGradient4615-3)" stroke-width=".26458"/>
<path d="m97.981 82.882-.0079.002c-.50121.31-.6214 89.64-.1211 90.138.17511.17499.44448.301.59766.27931 29.19-14.92 48.091-29.86 74.291-45.321 0-.84001-74.079-45.521-74.76-45.101z" fill="#fff" stroke-width=".26458"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=YouTube
Comment=Enjoy the videos and music you love, upload original content and share it all with friends, family and the world
Keywords=streaming;
Exec={{kiosk}} 'https://www.youtube.com'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=youtube \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
