#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Spotify.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Spotify.

License: MIT
DESCRIPTION

read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg width="256" height="256" version="1.1" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="128" cy="254.4" r="125.4" gradientTransform="matrix(2.038 0 0 2.028 -132.8 -261.4)" gradientUnits="userSpaceOnUse">
<stop stop-color="#a1c100" offset="0"/>
<stop stop-color="#7ea200" offset=".6282"/>
<stop stop-color="#a3c500" offset=".8702"/>
<stop stop-color="#aad000" offset="1"/>
</radialGradient>
</defs>
<path d="m128.1 2.641c-30.64-.01-76.87 5.19-98.54 26.84-21.68 21.65-26.91 67.87-26.92 98.51s5.19 76.86 26.84 98.54 67.87 26.91 98.51 26.92 76.87-5.18 98.54-26.84c21.68-21.65 26.91-67.87 26.92-98.51s-5.19-76.86-26.84-98.54-67.87-26.91-98.51-26.92z" fill="url(#a)"/>
<path d="m129.1 41.05c-48.58.000482-87.96 39.38-87.96 87.96-1.8e-5 48.58 39.38 87.96 87.96 87.96 48.58.00048 87.96-39.38 87.96-87.96 2e-5-48.58-39.38-87.97-87.96-87.96zm53.99 62.82c6.758 3.371 7.893 5.059 7.893 9.037 0 6.337-5.396 7.994-7.363 7.98-1.627-.0118-3.57-.8583-8.141-3.16-26.21-13.2-64.13-14.83-90.33-7.73-8.879 2.407-11.09-3.827-11.09-7.309.0022-3.507.7956-5.613 3.84-6.798 15.98-6.218 65.59-11.77 105.2 7.98zm-9.51 28.81c4.828 2.711 5.563 3.647 5.563 7.085 0 2.699-1.092 3.294-2.545 5.329l-.005.007c-.4312.6036-1.795.8057-2.983.8988-3.263.2558-20.09-11.23-45.94-13.91-24.59-2.543-38.74 4.436-43.4 3.614-3.231-.5702-7.316-6.305-2.678-10.72 4.002-3.81 52.33-14.58 91.99 7.693zm-46.56 13.32c24.08 2.194 39.99 12.34 41.31 14.22 1.849 2.629.281 7.977-4.514 7.622-4.28-.6018-14.17-9.784-37.31-11.62-21.62-1.715-34.64 3.746-39.73 2.688-4.892-1.016-3.877-6.311-2.916-7.597 2.264-3.029 26.53-6.826 43.17-5.31z" fill="#fff" style="paint-order:normal"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Spotify
Comment=Music for every moment, a digital music service that gives you access to millions of songs
Keywords=music;streaming;
Exec={{kiosk}} 'https://play.spotify.com'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Audio;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=spotify \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
