#!/bin/bash -e
: << DESCRIPTION
Generate a clean Mozilla Firefox profile preconfigured for Kick.

Both desktop file (a shortcut for this profile) and icon will be created
for convenience.

Firefox profile features:
* Tabs and menubar will autohide.
* Desktop link will open Kick.

License: MIT
DESCRIPTION

# directory definitions
profileDir="$HOME/.mozilla/firefox/firefox-kick"
applicationsDir="$HOME/.local/share/applications"
iconsDir="$HOME/.local/share/icons"

# file definitions
read -d '' iconFile << 'EOF' ||:
<?xml version="1.0" encoding="UTF-8"?>
<svg version="1.1" viewBox="0 0 256 256" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
<defs>
<radialGradient id="a" cx="-165.3" cy="-7.289" r="83.21" gradientTransform="matrix(3.434 0 -9.787e-8 3.428 702.2 280)" gradientUnits="userSpaceOnUse">
<stop stop-color="#313538" offset="0"/>
<stop stop-color="#141517" offset=".7"/>
<stop stop-color="#141517" offset="1"/>
</radialGradient>
</defs>
<path d="m253.4 128.1c-.0129 30.64-5.245 76.86-26.92 98.51-21.67 21.66-67.9 26.85-98.54 26.84-30.64-.01285-76.86-5.245-98.51-26.92-21.65-21.68-26.85-67.9-26.84-98.54.0128-30.64 5.245-76.86 26.92-98.51 21.68-21.65 67.9-26.85 98.54-26.84 30.64.0128 76.86 5.245 98.51 26.92 21.65 21.68 26.85 67.9 26.84 98.54z" fill="url(#a)" stroke-width="0"/>
<path d="m60.56 47.93v158.9h51.02v-35.26h16.98v17.63h16.98v17.63h51.02v-52.99h-16.98v-17.63h-16.98v-17.63h16.98v-17.63h16.98v-52.99h-51.02v17.63h-16.98v17.63h-16.98v-35.26h-51.02z" fill="#53fc18" stroke-width="0"/>
</svg>
EOF

read -d '' desktopFile << 'EOF' ||:
[Desktop Entry]
Name=Kick
Comment=Find and watch your favorite content
Keywords=games;streaming;
Exec={{kiosk}} 'https://kick.com/'
Terminal=false
Icon={{icon}}
StartupNotify=true
Type=Application
Categories=AudioVideo;Video;Network;Player;
EOF

kiosk=firefox-kiosk.sh
kiosk_url="https://gitlab.com/ergoithz/desktop-entry-scripts/-/raw/master/${kiosk}?inline=false"
(cat "$kiosk" || curl "$kiosk_url") | env \
  FIREFOX_KIOSK_APP=kick \
  FIREFOX_KIOSK_ICON="$iconFile" \
  FIREFOX_KIOSK_DESKTOP="$desktopFile" \
  bash
